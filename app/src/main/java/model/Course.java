package model;

import java.util.Date;
import java.util.List;

public class Course{

  private String hash;
  private int code;
  private Date opening;
  private String name;
  private String description;
  private int image;
  private List<Session> listSession;
  private List<Student> listStudent;
  private Teacher teacher;


  public Course(String hash, int code, Date opening, String name, String description, int image, List<Session> listSession, List<Student> listStudent, Teacher teacher) {
    this.hash = hash;
    this.code = code;
    this.opening = opening;
    this.name = name;
    this.description = description;
    this.image = image;
    this.listSession = listSession;
    this.listStudent = listStudent;
    this.teacher = teacher;
  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  public Date getOpening() {
    return opening;
  }

  public void setOpening(Date opening) {
    this.opening = opening;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getImage() {
    return image;
  }

  public void setImage(int image) {
    this.image = image;
  }

  public List<Session> getListSession() {
    return listSession;
  }

  public void setListSession(List<Session> listSession) {
    this.listSession = listSession;
  }

  public List<Student> getListStudent() {
    return listStudent;
  }

  public void setListStudent(List<Student> listStudent) {
    this.listStudent = listStudent;
  }

  public Teacher getTeacher() {
    return teacher;
  }

  public void setTeacher(Teacher teacher) {
    this.teacher = teacher;
  }

  public String getHash() {
    return hash;
  }

  public void setHash(String hash) {
    this.hash = hash;
  }
}