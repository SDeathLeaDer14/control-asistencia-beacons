package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by student on 3/12/17.
 */

@Dao
public interface CoursePersonDao {

    @Insert
    void insert(CoursePersonEntity coursePersonEntity);

    @Query("SELECT * FROM CourseEntity " +
            "INNER JOIN CoursePersonEntity ON CourseEntity.id = CoursePersonEntity.id_course " +
            "WHERE CoursePersonEntity.id_person = :idPerson")
    List<CourseEntity> getCourseEByPersonE(int idPerson);
}
