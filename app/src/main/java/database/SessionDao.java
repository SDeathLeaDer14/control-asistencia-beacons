package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;

import java.util.List;

/**
 * Created by student on 3/12/17.
 */

@Dao
public interface SessionDao {

    @Insert
    void insertAll(List<SessionEntity> sessionEntityList);

}
