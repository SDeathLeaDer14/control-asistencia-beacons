package database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by student on 16/12/17.
 */

@Dao
public interface CourseSessionDao {

    @Insert
    void insert(CourseSessionEntity courseSessionEntity);

    @Query("SELECT * FROM SessionEntity " +
            "INNER JOIN CourseSessionEntity ON SessionEntity.id = CourseSessionEntity.id_session " +
            "INNER JOIN CourseEntity ON CourseEntity.id = CourseSessionEntity.id_course " +
            "WHERE CourseEntity.name LIKE :nameCourse")
    List<SessionEntity> getSessionByNameCourse(String nameCourse);
}
