package com.example.veox.control_asitencia_beacons;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.llContainer) LinearLayout llContainer;
    Context c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        ButterKnife.bind(this);
        c = this;
        draw();
    }


    public void draw(){
        llContainer.setPadding(150, 0, 150, 0);
        llContainer.setWeightSum(100);
        llContainer.setGravity(Gravity.CENTER);

        final LinearLayout llEmail = new LinearLayout(c);
        llEmail.setGravity(Gravity.CENTER);
        llEmail.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 25));
        final EditText etEmail = new EditText(c);
        etEmail.setHint("Email");
        etEmail.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        llEmail.addView(etEmail);

        final LinearLayout llPassword = new LinearLayout(c);
        llPassword.setGravity(Gravity.CENTER);
        llPassword.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 25));
        final EditText etPassword = new EditText(c);
        etPassword.setHint("Contraseña");
        etPassword.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        llPassword.addView(etPassword);

        final LinearLayout llBegin = new LinearLayout(c);
        llBegin.setGravity(Gravity.CENTER);
        llBegin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 25));
        Button bBegin = new Button(c);
        bBegin.setText("Iniciar");
        bBegin.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        llBegin.addView(bBegin);

        bBegin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = etEmail.getText().toString();
                String password = etPassword.getText().toString();

                SharedPreferences sp = getSharedPreferences("Secreto", MODE_PRIVATE);
                SharedPreferences.Editor e = sp.edit();
                e.putString("email", email);
                e.putString("password", password);
                e.commit();

                Intent i = new Intent(c, RegisterActivity.class);
                startActivity(i);
            }
        });

        final LinearLayout llForgotPassword = new LinearLayout(c);
        llForgotPassword.setGravity(Gravity.CENTER);
        llForgotPassword.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 25));
        TextView tvForgotPassword = new TextView(c);
        tvForgotPassword.setText("Olvido su contraseña");
        tvForgotPassword.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        llForgotPassword.addView(tvForgotPassword);


        tvForgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                llEmail.setVisibility(View.GONE);
                llPassword.setVisibility(View.GONE);
                llBegin.setVisibility(View.GONE);
                llForgotPassword.setVisibility(View.GONE);


                final EditText etEmail = new EditText(c);
                etEmail.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));


                final Button bRecover = new Button(c);
                bRecover.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

                bRecover.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        etEmail.setVisibility(View.GONE);
                        bRecover.setVisibility(View.GONE);

                        llEmail.setVisibility(View.VISIBLE);
                        llPassword.setVisibility(View.VISIBLE);
                        llBegin.setVisibility(View.VISIBLE);
                        llForgotPassword.setVisibility(View.VISIBLE);
                    }
                });
                llContainer.addView(etEmail);
                llContainer.addView(bRecover);
            }
        });
        llContainer.addView(llEmail);
        llContainer.addView(llPassword);
        llContainer.addView(llBegin);
        llContainer.addView(llForgotPassword);


        Button bRegister = (Button)findViewById(R.id.bRegister);
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(c, RegisterActivity.class);
                startActivity(i);
            }
        });


    }
}
