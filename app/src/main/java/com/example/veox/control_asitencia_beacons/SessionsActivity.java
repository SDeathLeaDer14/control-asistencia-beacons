package com.example.veox.control_asitencia_beacons;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import database.MyDatabase;
import database.SessionEntity;
import model.Session;

public class SessionsActivity extends AppCompatActivity {

    ArrayList<Session> listSessions;
    Context c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sessions);
        Log.i("create-SessionsActivity", "Entro");
        final String nameCourse = getIntent().getStringExtra("nameCourse");
        c = this;
        Toast.makeText(c, "Lo estoy controlando", Toast.LENGTH_LONG).show();

        new Thread(new Runnable() {
            @Override
            public void run() {
                List<SessionEntity> listSessionE = MyDatabase.getInstance(null).courseSessionDao().getSessionByNameCourse(nameCourse);
                loadData(listSessionE);
                ListView lvSessions = (ListView) findViewById(R.id.lvSessions);

                CustomAdapterSession cAdapterSession = new CustomAdapterSession(c, listSessions);
                lvSessions.setAdapter(cAdapterSession);

                String course = getIntent().getStringExtra("aCourse");
                TextView tvTitle = (TextView)findViewById(R.id.tvTitle);
                tvTitle.setText(course);
            }
        }).start();



    }

    void loadData(List<SessionEntity> listSessionE){
        listSessions = new ArrayList<Session>();
        for(int i = 0; i<listSessionE.size(); i++){
            SessionEntity sessionE = listSessionE.get(i);
            listSessions.add(new Session(sessionE.getName(), new Date(sessionE.getDate()),
                    new Date(sessionE.getCheck_in()) , new Date(sessionE.getCheck_out())));
        }
    }



}
