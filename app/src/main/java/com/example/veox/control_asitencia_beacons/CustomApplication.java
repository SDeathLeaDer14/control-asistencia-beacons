package com.example.veox.control_asitencia_beacons;

import android.app.Application;
import android.arch.persistence.room.Room;

import database.MyDatabase;

/**
 * Created by student on 2/12/17.
 */

public class CustomApplication extends Application {

    MyDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();
        db = MyDatabase.getInstance(getApplicationContext());

    }
}
