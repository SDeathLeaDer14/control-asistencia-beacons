package com.example.veox.control_asitencia_beacons;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

import database.MyDatabase;

public class SplashActivity extends AppCompatActivity {

    int time = 0;
    public static MyDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        final ProgressBar pbLoading = (ProgressBar) findViewById(R.id.pbLoading);
        final Context c = this;

        SharedPreferences sp = getSharedPreferences("Secreto", MODE_PRIVATE);
        String email  = sp.getString("email", "None");
        String password = sp.getString("password", "None");

        // Crea la base de datos
        db = Room.databaseBuilder(c, MyDatabase.class, "database").build();


        if(email.equals("None") == true && password.equals("None") == true){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while(time <= 3){
                        try {
                            pbLoading.setProgress(time);
                            Thread.sleep(1000);
                            time  = time + 1;
                        }catch (InterruptedException i){
                            // error
                        }
                    }


                    Intent i = new Intent(c, LoginActivity.class);
                    startActivity(i);
                }
            }).start();
        } else{
            Intent i = new Intent(c, DashboardActivity.class);
            startActivity(i);
        }
    }
}
