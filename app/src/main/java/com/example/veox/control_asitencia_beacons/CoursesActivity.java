package com.example.veox.control_asitencia_beacons;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import database.CourseEntity;
import database.CoursePersonEntity;
import database.CourseSessionEntity;
import database.MyDatabase;
import database.SessionEntity;
import model.Course;
import model.Session;

public class CoursesActivity extends AppCompatActivity {

    private ArrayList<Course> listCourses;
    private ArrayList<CourseEntity> listCoursesE;
    private ArrayList<SessionEntity> listSessionE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_courses);
        Context c = this;

        ListView lvCourse = (ListView)findViewById(R.id.lvCourses);
        loadData();
        CustomAdapterCourse aaTest2 = new CustomAdapterCourse(c,  listCourses);
        lvCourse.setAdapter(aaTest2);

        new Thread(new Runnable() {
            @Override
            public void run() {
                MyDatabase database = MyDatabase.getInstance(null);
                database.courseDao().insertAll(listCoursesE);
                //database.courseDao().getCourseByNameAndDescription("Android", "%economico%");
                try {
                    database.coursePersonDao().insert(new CoursePersonEntity(1, 1));
                    database.coursePersonDao().insert(new CoursePersonEntity(2, 1));
                    database.coursePersonDao().insert(new CoursePersonEntity(3, 1));
                    database.coursePersonDao().insert(new CoursePersonEntity(1, 2));
                    database.coursePersonDao().insert(new CoursePersonEntity(2, 2));


                } catch (Exception e){
                    Log.i("Error", e.toString());
                    Log.i("Error", "No se se puede insertar");
                }

                database.sessionDao().insertAll(listSessionE);
                try {
                    database.courseSessionDao().insert(new CourseSessionEntity(1, 1));
                    database.courseSessionDao().insert(new CourseSessionEntity(1, 2));
                    database.courseSessionDao().insert(new CourseSessionEntity(1, 3));
                    database.courseSessionDao().insert(new CourseSessionEntity(1, 1));
                    database.courseSessionDao().insert(new CourseSessionEntity(1, 2));
                    database.courseSessionDao().insert(new CourseSessionEntity(1, 3));
                    database.courseSessionDao().insert(new CourseSessionEntity(1, 1));
                    database.courseSessionDao().insert(new CourseSessionEntity(1, 2));
                    database.courseSessionDao().insert(new CourseSessionEntity(1, 3));



                } catch (Exception e){
                    Log.i("Error", e.toString());
                    Log.i("Error", "No se se puede insertar");
                }

                List<CourseEntity> listCoursesE = database.coursePersonDao().getCourseEByPersonE(2);
                for (int i=0; i<listCoursesE.size(); i++){
                    CourseEntity courseE = listCoursesE.get(i);
                    listCourses.add(new Course("aa", courseE.getCode(), new Date(courseE.getOpening()),
                            courseE.getName(), courseE.getDescription(), courseE.getId_iname(),
                            null, null, null));
                }
            }
        }).start();
    }

     public void loadData(){



         listCourses = new ArrayList<>();

         //Insertar cursos
         listCoursesE = new ArrayList<>();
         listCoursesE.add(new CourseEntity(10001, new Date("12/12/17").toString(), "Android", "Es un curso de Android economico y practico", R.drawable.android));
         listCoursesE.add(new CourseEntity(10002, new Date("13/12/17").toString(), "IOS", "Es un curso economico y teorico", R.drawable.android));
         listCoursesE.add(new CourseEntity(10003, new Date("14/12/17").toString(), "Android con Firebase", "Es un curso  practico", R.drawable.android));
         listCoursesE.add(new CourseEntity(10004, new Date("15/12/17").toString(), "Android con Clean Arquitecture", "Es un curso  practico", R.drawable.android));
         listCoursesE.add(new CourseEntity(10005, new Date("16/12/17").toString(), "Android con Unity", "Es un curso economico practico", R.drawable.android));

         //Insertar sesiones
         listSessionE = new ArrayList<>();
         listSessionE.add(new SessionEntity(
                 "Sesion I",
                 new Date("11/11/17").toString(),
                 new Date("11/11/17").toString(),
                 new Date("11/11/17").toString(),
                 0));
         listSessionE.add(new SessionEntity("Sesion II", new Date("11/11/17").toString(), new Date("11/11/17").toString(), new Date("11/11/17").toString(), 0));
         listSessionE.add(new SessionEntity("Sesion III", new Date("11/11/17").toString(), new Date("11/11/17").toString(), new Date("11/11/17").toString(), 0));
         listSessionE.add(new SessionEntity("Sesion IV", new Date("11/11/17").toString(), new Date("11/11/17").toString(), new Date("11/11/17").toString(), 0));
         listSessionE.add(new SessionEntity("Sesion V", new Date("11/11/17").toString(), new Date("11/11/17").toString(), new Date("11/11/17").toString(), 0));
         listSessionE.add(new SessionEntity("Sesion VI", new Date("11/11/17").toString(), new Date("11/11/17").toString(), new Date("11/11/17").toString(), 0));

     }

}
